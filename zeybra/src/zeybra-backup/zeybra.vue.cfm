<template>
  <div class="container z-container">
    <!-- Modal -->
    <div class="">
      <div
        class="modal fade"
        id="configureFieldModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="configureFieldModal"
        aria-hidden="true"
      >
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Configure Object Fields</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body z-modal-body">
              <div class="form-field">
                <label>Current filed type: {{ getFieldTypeLabel(selectedField.field_type) }}</label>
              </div>
              <div class="form-field">
                <label>Change field type</label>
                <select name="connect" id="connect" v-model="selectedFieldType">
                  <option value="">-Select-</option>
                  <option v-for="(field, index) in hubspotFieldTypes" :key="index" :value="field.value">
                    {{ field.label }}
                  </option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="l-btn cancel" data-dismiss="modal">Cancel</button>
              <button
                type="button"
                class="l-btn"
                data-dismiss="modal"
                @click="configureField(selectedField, selectedFieldType)"
              >
                Configure
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Progress Bar -->
    <div class="zy process-nav">
      <ul>
        <li :class="{ active: step === CONNECT || step === OBJECT_MAP || step === SYNC }">
          <div class="step-item"><span>1</span> <i class="far fa-check-circle"></i></div>
          <h3>Connect</h3>
        </li>
        <li :class="{ active: step === OBJECT_MAP || step === SYNC }">
          <div class="step-item"><span>2</span><i class="far fa-check-circle"></i></div>
          <h3>Map</h3>
        </li>
        <li
          :class="{
            'first-quarter': this.step === OBJECT_MAP && this.mapStep === CUSTOMER,
            'second-quarter': this.step === OBJECT_MAP && this.mapStep === PRODUCT,
            active: this.step === SYNC,
          }"
        >
          <div class="step-item"><span>3</span><i class="far fa-check-circle"></i></div>
          <h3>Sync</h3>
        </li>
      </ul>
    </div>
    <!-- Connect Page -->
    <div class="zy connect" v-if="step === CONNECT">
      <div class="hero-text">
        <h1>Connect Your Stripe Account</h1>
      </div>
      <div class="bg-grey">
        <div class="card card--connect">
          <div class="connect__card-wrap">
            <div class="connect__thumb stripe__thumb">
              <img src="/static/zeybra/images/Stripe-logo.png" alt="Logo" />
            </div>
            <div class="connect__toggle">
              <label class="l-toggle">
                <input type="checkbox" v-model="hasStripeLogin" disabled /><span class="l-toggle-slider round"></span>
              </label>
            </div>
          </div>
          <div class="connect__wrap">
            <div class="form-field">
              <input
                type="text"
                name="public-key"
                id="public-key"
                v-model="stripe.publicKey"
                placeholder="Stripe Public Key"
              />
              <p v-if="errors.publicKey" class="err_text has-text-danger has-text-left">
                {{ errors.publicKey }}
              </p>
            </div>
            <div class="form-field">
              <input
                type="text"
                name="secret-key"
                id="secret-key"
                v-model="stripe.secretKey"
                placeholder="Stripe Secret Key"
              />
              <p v-if="errors.secretKey" class="err_text has-text-danger has-text-left">{{ errors.secretKey }}</p>
            </div>
          </div>
        </div>
      </div>
      <div class="next__wrap next_single_wrap">
        <button class="l-btn" title="button" @click="loginToStripe" :disabled="loading">
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>
    <!-- Choose Enterprise Customer-->
    <div class="zy customer" v-if="step === OBJECT_MAP && mapStep === ENTERPRISE">
      <div class="hero-text">
        <h1>Choose Your Customer Type</h1>
      </div>
      <div class="customer__wrap">
        <!-- <h2>Choose our customer type?</h2> -->
        <label class="l-radio"
          >Enterprise Customers
          <input type="radio" name="enterpriseCustomer" id="yes" value="yes" v-model="isEnterpriseCustomer" /><span
            class="checkmark"
          ></span>
        </label>
        <label class="l-radio"
          >Non-Enterprise Customers
          <input type="radio" name="enterpriseCustomer" id="no" value="no" v-model="isEnterpriseCustomer" /><span
            class="checkmark"
          ></span>
        </label>
      </div>
      <div class="next__wrap next_sm_wrap">
        <button class="l-btn back" title="button" @click="navigateSteps(CONNECT)">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button class="l-btn" type="button" @click="saveCustomerType" :disabled="loading">
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>
    <!-- Customer Map Page -->
    <div class="zy map" v-if="step === OBJECT_MAP && mapStep === CUSTOMER">
      <div class="hero-text">
        <h1>Map Your Fields From Stripe</h1>
      </div>
      <div class="bg-grey">
        <div class="card card--map">
          <div class="map_wrap" v-for="(field, index) in stripeFieldList" :key="index">
            <div class="map_fields">
              <div class="form-field form-field--rounded">
                <input type="text" placeholder="Hubspot Field Name" v-model="field.label" :disabled="field.reserved" />
              </div>
              <div class="form-field" :class="{ 'form-field--rounded': true }">
                <input type="text" placeholder="Stripe Field Name" :value="field.stripe_key" disabled />
              </div>
              <div class="map__button">
                <button
                  class="l-configure"
                  data-toggle="modal"
                  @click="toggleModal(field)"
                  data-target="#configureFieldModal"
                  :disabled="field.reserved"
                >
                  Configure
                </button>
                <button
                  class="l-modify"
                  href="#"
                  title="button"
                  @click="deleteFieldObjects(index)"
                  :disabled="field.reserved"
                >
                  <i class="fas fa-trash-alt"></i>
                </button>
              </div>
            </div>
          </div>
          <p v-if="errors.fieldMapError" class="err_text has-text-danger has-text-left">
            {{ errors.fieldMapError }}
          </p>
        </div>
      </div>
      <div class="next__wrap">
        <button class="l-btn back" title="button" @click="navigateSteps(OBJECT_MAP, ENTERPRISE)">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button class="l-btn" type="button" @click="mapCustomerObjects" :disabled="loading">
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>

    <!-- Product Map Page -->
    <div class="zy product" v-if="step === OBJECT_MAP && mapStep === PRODUCT">
      <div class="hero-text">
        <h1>Map Your Products</h1>
      </div>
      <div class="bg-grey">
        <div class="card card--product">
          <div class="product__wrap button__wrap">
            <div class="product__left">
              <div class="product__thumb"><img src="/static/zeybra/images/Hubspot-logo.png" alt="Logo" /></div>
              <div class="form-field">
                <select ref="product_input" name="connect" id="connect" v-model="hubspotProduct">
                  <option value="">-Select-</option>
                  <option v-for="(product, index) in hubspotProductList" :key="index" :value="product">
                    {{ product.properties.name }}<i class="baseline-near_me"></i>
                  </option>
                </select>
              </div>
            </div>
            <div class="product__right">
              <div class="product__thumb stripe__thumb">
                <img src="/static/zeybra/images/Stripe-logo.png" alt="Logo" />
              </div>
              <div class="form-field">
                <select name="connect" id="connect" v-model="stripeProduct">
                  <option value="">-Select-</option>
                  <option v-for="(product, index) in stripeProductList" :key="index" :value="product">
                    {{ product.name }}<i class="baseline-near_me"></i>
                  </option>
                </select>
              </div>
            </div>
            <div class="form-field">
              <button
                class="l-add-more"
                type="button"
                :disabled="!hubspotProduct || !stripeProduct"
                @click="addObjectProperties({ hubspotProduct, stripeProduct })"
              >
                <i class="fas fa-plus"></i>
              </button>
            </div>
          </div>
          <p v-if="errors.hubspotProduct" class="err_text has-text-danger has-text-left">
            {{ errors.hubspotProduct }}
          </p>
          <!-- Added Products -->
          <div class="product__wrap button__wrap" v-for="(product, index) in selectedProducts" :key="index">
            <div class="product__left">
              <div class="form-field">
                <input type="text" v-model="product.hubspotProduct.properties.name" disabled />
              </div>
            </div>
            <div class="product__right">
              <div class="form-field">
                <input type="text" v-model="product.stripeProduct.name" disabled />
              </div>
            </div>
            <div class="form-field">
              <button class="l-minus" type="button" @click="deleteProductSelection(index)">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="next__wrap">
        <button class="l-btn back" title="button" @click="navigateSteps(OBJECT_MAP, CUSTOMER)">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button
          class="l-btn"
          type="button"
          @click="mapProductObjects"
          :disabled="loading || selectedProducts.length < 1"
        >
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>

    <!-- Subscription Map Page -->
    <div class="zy map" v-if="step === OBJECT_MAP && mapStep === 'subscription'">
      <div class="hero-text">
        <h1>Map Your Subscriptions From Stripe</h1>
      </div>
      <div class="bg-grey">
        <div class="card card--map">
          <div class="map_wrap" v-for="(field, index) in stripeSubscriptionList" :key="index">
            <div class="map_fields">
              <div class="form-field form-field--rounded">
                <input
                  type="text"
                  placeholder="Hubspot Subscription"
                  v-model="field.label"
                  :disabled="field.reserved"
                />
              </div>
              <div class="form-field" :class="{ 'form-field--rounded': true }">
                <input type="text" placeholder="Stripe Subscription" :value="field.stripe_key" disabled />
              </div>
              <div class="map__button">
                <button
                  class="l-configure"
                  data-toggle="modal"
                  @click="toggleModal(field)"
                  data-target="#configureFieldModal"
                  :disabled="field.reserved"
                >
                  Configure
                </button>
                <button
                  class="l-modify"
                  href="#"
                  title="button"
                  @click="deleteSubscriptionObjects(index)"
                  :disabled="field.reserved"
                >
                  <i class="fas fa-trash-alt"></i>
                </button>
              </div>
            </div>
          </div>
          <p v-if="errors.subscriptionError" class="err_text has-text-danger has-text-left">
            {{ errors.subscriptionError }}
          </p>
        </div>
      </div>
      <div class="next__wrap">
        <button class="l-btn back" title="button" @click="navigateSteps(OBJECT_MAP, PRODUCT)">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button class="l-btn" type="button" @click="mapSubscriptionObjects" :disabled="loading">
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>

    <!-- Transaction Map Page -->
    <div class="zy map" v-if="step === OBJECT_MAP && mapStep === 'transaction'">
      <div class="hero-text">
        <h1>Map Your Transaction From Stripe</h1>
      </div>
      <div class="bg-grey">
        <div class="card card--map">
          <div class="map_wrap" v-for="(field, index) in stripeTransactionList" :key="index">
            <div class="map_fields">
              <div class="form-field form-field--rounded">
                <input type="text" placeholder="Hubspot Transaction" v-model="field.label" :disabled="field.reserved" />
              </div>
              <div class="form-field" :class="{ 'form-field--rounded': true }">
                <input type="text" placeholder="Stripe Transaction" :value="field.stripe_key" disabled />
              </div>
              <div class="map__button">
                <button
                  class="l-configure"
                  data-toggle="modal"
                  @click="toggleModal(field)"
                  data-target="#configureFieldModal"
                  :disabled="field.reserved"
                >
                  Configure
                </button>
                <button
                  class="l-modify"
                  href="#"
                  title="button"
                  @click="deleteFieldObjects(index)"
                  :disabled="field.reserved"
                >
                  <i class="fas fa-trash-alt"></i>
                </button>
              </div>
            </div>
          </div>
          <p v-if="errors.transactionError" class="err_text has-text-danger has-text-left">
            {{ errors.transactionError }}
          </p>
        </div>
      </div>
      <div class="next__wrap">
        <button class="l-btn back" title="button" @click="navigateSteps(OBJECT_MAP, 'subscription')">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button class="l-btn" type="button" @click="mapTransactionObjects" :disabled="loading">
          Next<i class="fas fa-arrow-circle-right" v-if="!loading"></i>
          <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>

    <!-- Sync Page -->
    <div class="zy loader" v-if="step === SYNC">
      <div class="loader__sucess" v-if="syncStatus === COMPLETED">
        <div class="success__wrap"><i class="far fa-check-circle"></i></div>
        <div class="loader__mesage">
          <h2>Sync completed</h2>
        </div>
      </div>
      <div class="loader__sucess" v-if="syncStatus === CANCELLED">
        <div class="cancel__wrap"><i class="fas fa-times-circle"></i></div>
        <div class="loader__mesage">
          <h2>Sync cancelled</h2>
        </div>
      </div>
      <div class="loader__wrap" v-if="syncStatus === PROGRESS">
        <div class="lds-ring" :class="{ 'static-ring': syncStatus === READY }">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div class="loader__mesage">
          <h2>Sync is in progress</h2>
        </div>
      </div>
      <div class="loader__wrap" v-if="syncStatus === READY">
        <div class="static-ring">
          <div></div>
        </div>
        <div class="loader__mesage">
          <h2>Ready to sync</h2>
        </div>
      </div>
      <div class="next__wrap next_sm_wrap">
        <button class="l-btn back" type="button" @click="navigateSteps(OBJECT_MAP, PRODUCT)" :disabled="syncStatus === PROGRESS">
          <i class="fas fa-arrow-circle-left"></i>Back
        </button>
        <button class="l-btn cancel" type="button" v-if="syncStatus === PROGRESS" @click="cancelObjectSync()" :disabled="loading">
          Cancel<i class="fas fa-times" v-if="!loading"></i><i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
        <button class="l-btn" type="button" @click="startObjectSync()" :disabled="loading" v-else>
          Start Sync<i class="fas fa-sync" v-if="!loading"></i> <i class="fas fa-spinner" aria-hidden="true" v-else></i>
        </button>
      </div>
    </div>
  </div>
</template>

<style scoped src="/static/zeybra/css/style.css"></style>
<style scoped src="/static/zeybra/css/sb-admin-2.css"></style>

<script>
/**Constants*/
const METHOD_POST = "POST";
const METHOD_GET = "GET";
const READY = "READY";
const PROGRESS = "PROGRESS";
const CANCELLED = "CANCELLED";
const COMPLETED = "COMPLETED";
const OBJECT_MAP = "OBJECT_MAP";
const CONNECT = "CONNECT";
const SYNC = "SYNC";
const ENTERPRISE = "ENTERPRISE";
const CUSTOMER = "CUSTOMER";
const PRODUCT = "PRODUCT";
/** Vue component for Zaybra */
module.exports = {
  name: "Zaybra",
  data() {
    return {
      step: CONNECT,
      mapStep: ENTERPRISE,
      stripe: {
        secretKey: "",
        publicKey: "",
      },
      loading: false,
      polling: null,
      selectedProducts: [],
      hubspotProduct: "",
      stripeProduct: "",
      errors: {
        hubspotProduct: "",
        secretKey: "",
        publicKey: "",
        fieldMapError: "",
      },
      hubspotProductList: [],
      stripeProductList: [],
      stripeFieldList: [],
      stripeTransactionList: [],
      hubspotTransactionList: [],
      stripeSubscriptionList: [],
      hubspotSubscriptionList: [],
      userId: "",
      apiToken: "",
      baseUrl: "",
      hasStripeLogin: false,
      syncStatus: READY,
      isEnterpriseCustomer: "yes",
      selectedField: {},
      selectedFieldType: "",
      customerSyncId: "",
      productSyncId: "",
      hubspotFieldTypes: [
        { label: "Text", value: "text" },
        { label: "Textarea", value: "textarea" },
        { label: "Boolean Checkbox", value: "booleancheckbox" },
        { label: "Checkbox", value: "checkbox" },
        { label: "Date", value: "date" },
        { label: "File", value: "file" },
        { label: "Number", value: "number" },
        { label: "Radio", value: "radio" },
        { label: "Select", value: "select" },
      ],
    };
  },
  props: {
    baseurl: String,
    inuserid: String,
    inapitoken: String,
  },
  computed: {},
  created() {
    this.userId = this.inuserid;
    this.apiToken = this.inapitoken;
    this.baseUrl = this.baseurl;
    this.READY = READY;
    this.PROGRESS = PROGRESS;
    this.CANCELLED = CANCELLED;
    this.COMPLETED = COMPLETED;
    this.OBJECT_MAP = OBJECT_MAP;
    this.CONNECT = CONNECT;
    this.SYNC = SYNC;
    this.ENTERPRISE = ENTERPRISE;
    this.CUSTOMER = CUSTOMER;
    this.PRODUCT = PRODUCT;
  },
  mounted() {
    this.getStripeKeys();
    this.checkSyncProgress();
  },
  beforeDestroy() {
    clearInterval(this.polling);
  },
  methods: {
    // Navigate across steps
    navigateMainSteps(step) {
      switch (step) {
        case CONNECT: {
          this.step = CONNECT;
          this.hasStripeLogin = false;
          this.loading = false;
          this.getStripeKeys();
          break;
        }
        case OBJECT_MAP: {
          this.loading = false;
          this.step = OBJECT_MAP;
          break;
        }
        case SYNC:
          this.step = SYNC;
          break;
        default:
          this.step = CONNECT;
          break;
      }
    },
    // Navigate across mapping steps
    navigateMapSteps(step) {
      switch (step) {
        case ENTERPRISE: {
          this.getCustomerType();
          this.mapStep = ENTERPRISE;
          this.loading = false;
          break;
        }
        case CUSTOMER: {
          this.getStripeFieldList();
          this.mapStep = CUSTOMER;
          this.loading = false;
          break;
        }
        case PRODUCT: {
          this.getInitProductData();
          this.mapStep = PRODUCT;
          this.loading = false;
          break;
        }
        default:
          this.mapStep = ENTERPRISE;
          break;
      }
    },
    navigateSteps(mainStep, mapStep) {
      this.navigateMainSteps(mainStep);
      if (mapStep) {
        this.navigateMapSteps(mapStep);
      }
    },
    changeSyncStatus(status) {
      switch (status) {
        case READY:
          this.syncStatus = READY;
          break;
        case PROGRESS: {
          this.navigateSteps(SYNC);
          this.syncStatus = PROGRESS;
          break;
        }
        case COMPLETED:
          this.syncStatus = COMPLETED;
          break;
        case CANCELLED:
          this.syncStatus = CANCELLED;
          break;
        default:
          this.syncStatus = READY;
          break;
      }
    },
    /** POST APIs */
    loginToStripe() {
      this.loading = true;
      let hasErrors = false;
      const { secretKey, publicKey } = this.stripe;
      if (!secretKey) {
        this.errors.secretKey = "Please enter stripe Secret Key";
        hasErrors = true;
        this.loading = false;
      }

      if (!publicKey) {
        this.errors.publicKey = "Please enter stripe Public Key";
        hasErrors = true;
        this.loading = false;
      }

      if (!hasErrors) {
        this.errors.secretKey = "";
        this.errors.publicKey = "";
        const data = {
          sk: secretKey,
          pk: publicKey,
        };

        const requestOptions = this.getRequestOptions(METHOD_POST, data);
        try {
          fetch(`${this.baseurl}/stripe/verify`, requestOptions)
            .then(async (response) => {
              const { code, data, message } = await response.json();
              if (code === 200) {
                if (!data.pk) {
                  this.errors.publicKey = message;
                  this.hasStripeLogin = false;
                } else if (!data.sk) {
                  this.errors.secretKey = message;
                  this.hasStripeLogin = false;
                } else {
                  this.hasStripeLogin = true;
                  this.navigateSteps(OBJECT_MAP, ENTERPRISE);
                }
              }
              this.loading = false;
            })
            .catch(function (error) {
              this.loading = false;
            });
        } catch (e) {
          this.loading = false;
          console.error(e);
        }
      }
    },
    saveCustomerType() {
      this.loading = true;
      const data = {
        enterprice: this.isEnterpriseCustomer,
      };

      const requestOptions = this.getRequestOptions(METHOD_POST, data);
      try {
        fetch(`${this.baseurl}/hubspot/enterprice`, requestOptions)
          .then(async (response) => {
            const { code } = await response.json();
            if (code === 200) {
              this.navigateSteps(OBJECT_MAP, CUSTOMER);
            }
            this.loading = false;
          })
          .catch(function (error) {
            this.loading = false;
            console.error(error);
          });
      } catch (e) {
        this.loading = false;
        console.error(e);
      }
    },
    mapCustomerObjects() {
      this.loading = true;
      const fieldObjects = [];
      let hasMissingFields = false;

      this.stripeFieldList.forEach((field) => {
        let fieldObject = {
          hubspot_key: this.createSlug(field.label),
          stripe_key: field.stripe_key,
          field_type: field.field_type,
          label: field.label,
        };
        fieldObjects.push(fieldObject);
        if (!field.label) {
          hasMissingFields = true;
        }
      });

      if (hasMissingFields) {
        this.errors.fieldMapError = "Please map all your fields";
        this.loading = false;
      } else {
        try {
          const formData = JSON.parse(JSON.stringify(fieldObjects));
          const requestOptions = this.getRequestOptions(METHOD_POST, formData);
          fetch(`${this.baseurl}/hubspot/customer_object_map`, requestOptions)
            .then(async (response) => {
              const { code } = await response.json();
              if (code === 200) {
                this.navigateSteps(OBJECT_MAP, PRODUCT);
              }
              this.loading = false;
            })
            .catch(function (error) {
              this.loading = false;
              console.error(error);
            });
        } catch (e) {
          this.loading = false;
          console.error(e);
        }
      }
      this.selectedField = {};
    },
    deleteFieldObjects(index) {
      this.stripeFieldList.splice(index, 1);
    },
    mapProductObjects() {
      this.loading = true;
      let products = [];
      if (!this.selectedProducts.length) {
        this.errors.hubspotProduct = "No products were selected for mapping!!!";
      } else {
        this.selectedProducts.forEach((product) => {
          let object = {
            hubspot_key: product.hubspotProduct.id,
            stripe_key: product.stripeProduct.id,
          };
          products.push(object);
        });
        try {
          const requestOptions = this.getRequestOptions(METHOD_POST, products);
          fetch(`${this.baseurl}/hubspot/product_object_map`, requestOptions)
            .then(async (response) => {
              const { code } = await response.json();
              if (code === 200) {
                this.navigateSteps(SYNC);
              }
              this.errors.hubspotProduct = "";
              this.selectedProducts = [];
              this.loading = false;
            })
            .catch(function (error) {
              this.loading = false;
              console.error(error);
            });
        } catch (e) {
          this.loading = false;
          console.error(e);
        }
      }
    },
    deleteProductSelection(index) {
      this.selectedProducts.splice(index, 1);
    },
    mapSubscriptionObjects() {
      this.loading = true;
      const subscriptionObjects = [];
      let hasMissingFields = false;

      this.stripeSubscriptionList.forEach((field) => {
        let subscriptionObject = {
          hubspot_key: this.createSlug(field.label),
          stripe_key: field.stripe_key,
          field_type: field.field_type,
          label: field.label,
        };
        subscriptionObjects.push(subscriptionObject);
        if (!field.label) {
          hasMissingFields = true;
        }
      });

      if (hasMissingFields) {
        this.errors.subscriptionError = "Please map all your subscriptions";
        this.loading = false;
      } else {
        const formData = JSON.parse(JSON.stringify(subscriptionObjects));
        const requestOptions = this.getRequestOptions(METHOD_POST, formData);
        fetch(`${this.baseurl}/hubspot/subcription_object_map`, requestOptions)
          .then(async (response) => {
            const { code } = await response.json();
            if (code === 200) {
              this.navigateSteps(SYNC);
            }
            this.loading = false;
          })
          .catch(function (error) {
            this.loading = false;
            console.error(error);
          });
      }
      this.selectedField = {};
    },
    deleteSubscriptionObjects(index) {
      this.stripeSubscriptionList.splice(index, 1);
    },
    mapTransactionObjects() {
      this.loading = true;
      const transactionObjects = [];
      let hasMissingFields = false;

      this.stripeTransactionList.forEach((field) => {
        let transactionObject = {
          hubspot_key: this.createSlug(field.label),
          stripe_key: field.stripe_key,
          field_type: field.field_type,
          label: field.label,
        };
        transactionObjects.push(transactionObject);
        if (!field.label) {
          hasMissingFields = true;
        }
      });

      if (hasMissingFields) {
        this.errors.transactionError = "Please map all your transactions";
        this.loading = false;
      } else {
        const formData = JSON.parse(JSON.stringify(transactionObjects));
        const requestOptions = this.getRequestOptions(METHOD_POST, formData);

        fetch("", requestOptions)
          .then(async (response) => {
            const { code } = await response.json();
            if (code === 200) {
              this.navigateSteps(SYNC);
            }
            this.loading = false;
          })
          .catch(function (error) {
            this.loading = false;
            console.error(error);
          });
      }
      this.selectedField = {};
    },
    deleteTransactionObjects(index) {
      this.stripeTransactionList.splice(index, 1);
    },
    startObjectSync: function () {
      this.startSyncProcess();
      /**Polling the sync API in every 5 seconds */
      this.polling = setInterval(() => {
        this.checkSyncProgress();
      }, 5000);
    },
    addObjectProperties(mappedProduct) {
      let error = false;
      if (this.selectedProducts.length > 0) {
        this.selectedProducts.forEach((item) => {
          if (
            item.hubspotProduct.id === mappedProduct.hubspotProduct.id ||
            item.stripeProduct.id === mappedProduct.stripeProduct.id
          ) {
            this.errors.hubspotProduct = "Product already mapped !!!";
            error = true;
            return;
          }
        });
      }
      if (!error) {
        this.errors.hubspotProduct = "";
        this.selectedProducts.push(mappedProduct);
        this.hubspotProduct = "";
        this.stripeProduct = "";
        this.$refs.product_input.focus();
      }
    },
    startSyncProcess() {
      this.loading = true;
      const requestOptions = this.getRequestOptions(METHOD_POST);
      (async () => {
        try {
          const allResponses = await Promise.all([
            fetch(`${this.baseurl}/stripe/sync-products`, requestOptions).catch((err) => {
              console.error(err);
            }),
            fetch(`${this.baseurl}/stripe/sync-customers`, requestOptions).catch((err) => {
              console.error(err);
            }),
          ]);
          const productObjectSync = await allResponses.at(0).json();
          const customerObjectSync = await allResponses.at(1).json();
          if (productObjectSync.code === 200 || customerObjectSync.code === 200) {
            this.changeSyncStatus(PROGRESS);
            this.productSyncId = productObjectSync.data;
            this.customerSyncId = customerObjectSync.data;
          }
          this.loading = false;
        } catch (e) {
          console.error(e);
          this.loading = false;
        }
      })();
    },
    cancelObjectSync() {
      this.loading = true;
      const customerSyncData = {
        message_id: this.customerSyncId,
      };
      const productSyncData = {
        message_id: this.productSyncId,
      };

      const prodRequestOption = this.getRequestOptions(METHOD_POST, productSyncData);
      const custRequestOption = this.getRequestOptions(METHOD_POST, customerSyncData);

      (async () => {
        try {
          const allResponses = await Promise.all([
            fetch(`${this.baseurl}/stripe/cancel-products-sync`, prodRequestOption).catch((err) => {
              console.error(err);
            }),
            fetch(`${this.baseurl}/stripe/cancel-customers-sync`, custRequestOption).catch((err) => {
              console.error(err);
            }),
          ]);
          const cancelProductSync = await allResponses.at(0).json();
          const cancelCustomerSync = await allResponses.at(1).json();
          if (cancelProductSync.code === 200 && cancelCustomerSync.code === 200) {
            clearInterval(this.polling);
            this.changeSyncStatus(CANCELLED);
          }
          this.loading = false;
        } catch (e) {
          console.error(e);
          this.loading = false;
        }
      })();
    },
    checkSyncProgress() {
      const requestOptions = this.getRequestOptions(METHOD_POST);
      try {
        fetch(`${this.baseurl}/stripe/sync-status`, requestOptions)
          .then(async (response) => {
             const res = await response.json();
             const { customers, products } = res.data;
            if (res.code === 200) {
              if (products.status === 1 || customers.status === 1) {
                this.changeSyncStatus(PROGRESS);
              } else if (products.status === 0 && customers.status === 0) {
                this.changeSyncStatus(CANCELLED);
                clearInterval(this.polling);
              } else if (products.status === 2 && customers.status === 2) {
                this.changeSyncStatus(COMPLETED);
                clearInterval(this.polling);
              } else {
                this.changeSyncStatus(READY);
                clearInterval(this.polling);
              }
            }
            this.loading = false;
          })
          .catch(function (error) {
            this.loading = false;
            console.error(error);
          });
      } catch (e) {
        console.error(e);
        this.loading = false;
      }
    },
    /** GET APIs */
    getStripeKeys() {
      fetch(`${this.baseurl}/stripe/keys`, this.getRequestOptions(METHOD_GET)).then(async (response) => {
        const res = await response.json();
        if (res.code === 200) {
          const { sk, pk } = res?.data;
          this.stripe.secretKey = sk;
          this.stripe.publicKey = pk;
          this.hasStripeLogin = true;
        } else {
          this.stripe.secretKey = "";
          this.stripe.publicKey = "";
        }
      });
    },
    getCustomerType() {
      fetch(`${this.baseurl}/hubspot/enterprice`, this.getRequestOptions(METHOD_GET)).then(async (response) => {
        const res = await response.json();
        if (res.code === 200) {
          this.isEnterpriseCustomer = res.data;
        }
      });
    },
    getStripeFieldList() {
      this.stripeFieldList = [];
      let fieldList = [];
      fetch(`${this.baseurl}/hubspot/customer_object_map`, this.getRequestOptions(METHOD_GET)).then(
        async (response) => {
          const res = await response.json();
          if (res.code === 200) {
            const { contactObject, stripeObject } = res.data;
            for (const [key, value] of Object.entries(stripeObject)) {
              let field = {
                stripe_key: key,
                reserved: value.reserved,
                label: value.reserved ? key : "",
                field_type: "text",
              };
              if (contactObject.length) {
                contactObject.forEach((savedField) => {
                  if (savedField.stripe_key === key) {
                    field = {
                      stripe_key: savedField.stripe_key,
                      reserved: value.reserved,
                      label: savedField.custom_object_label,
                      field_type: savedField.custom_object_field_type,
                    };
                  }
                });
              }
              fieldList.push(field);
            }
            this.stripeFieldList = fieldList;
          }
        }
      );
    },
    getInitProductData() {
      (async () => {
        try {
          const allResponses = await Promise.all([
            fetch(`${this.baseurl}/stripe/products`, this.getRequestOptions(METHOD_GET)).catch((err) => {
              console.error(err);
            }),
            fetch(`${this.baseurl}/hubspot/products`, this.getRequestOptions(METHOD_GET)).catch((err) => {
              console.error(err);
            }),
            fetch(`${this.baseurl}/hubspot/product_object_map`, this.getRequestOptions(METHOD_GET)).catch((err) => {
              console.error(err);
            }),
          ]);
          const stripeProdData = await allResponses.at(0).json();
          const hubspotProdData = await allResponses.at(1).json();
          const {
            data: { productObject },
          } = await allResponses.at(2).json();
          this.stripeProductList = stripeProdData?.data?.data;
          this.hubspotProductList = hubspotProdData?.data;
          if (productObject.length) {
            productObject.forEach((prod) => {
              const savedHubProd = this.hubspotProductList.find((hub) => hub.id === prod.custom_object_key);
              const savedStripeProd = this.stripeProductList.find((hub) => hub.id === prod.stripe_key);
              this.addObjectProperties({ hubspotProduct: savedHubProd, stripeProduct: savedStripeProd });
            });
          }
        } catch (e) {
          console.error(e);
        }
      })();
    },
    getStripeSubscriptionList() {
      this.stripeFieldList = [];
      let fieldList = [];
      fetch(`${this.baseurl}/hubspot/subcription_object_map`, this.getRequestOptions(METHOD_GET)).then(
        async (response) => {
          const res = await response.json();
          if (res.code === 200) {
            const { subscriptionObject, productObject } = res.data;
            for (const [key, value] of Object.entries(subscriptionObject)) {
              let field = {
                stripe_key: key,
                reserved: value.reserved,
                label: value.reserved ? key : "",
                field_type: "text",
              };
              if (productObject.length) {
                productObject.forEach((savedField) => {
                  if (savedField.stripe_key === key) {
                    field = {
                      stripe_key: savedField.stripe_key,
                      reserved: value.reserved,
                      label: savedField.custom_object_label,
                      field_type: savedField.custom_object_field_type,
                    };
                  }
                });
              }
              fieldList.push(field);
            }
            this.stripeSubscriptionList = fieldList;
          }
        }
      );
    },
    getStripeTransactionList() {
      this.stripeFieldList = [];
      let fieldList = [];
      fetch("", this.getRequestOptions(METHOD_GET)).then(async (response) => {
        const res = await response.json();
        if (res.code === 200) {
          const { transactionObject, productObject } = res.data;
          for (const [key, value] of Object.entries(transactionObject)) {
            let field = {
              stripe_key: key,
              reserved: value.reserved,
              label: value.reserved ? key : "",
              field_type: "text",
            };
            if (productObject.length) {
              productObject.forEach((savedField) => {
                if (savedField.stripe_key === key) {
                  field = {
                    stripe_key: savedField.stripe_key,
                    reserved: value.reserved,
                    label: savedField.custom_object_label,
                    field_type: savedField.custom_object_field_type,
                  };
                }
              });
            }
            fieldList.push(field);
          }
          this.stripeTransactionList = fieldList;
        }
      });
    },
    toggleModal(field) {
      this.selectedField = field;
    },
    /**Util Methods */
    configureField(selectedField, selectedFieldType) {
      selectedField.field_type = selectedFieldType;
    },
    createSlug(slug) {
      return slug
        .toString()
        .trim()
        .toLowerCase()
        .replace(/\s+/g, "_")
        .replace(/[^\w\-]+/g, "")
        .replace(/\-\-+/g, "_")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
    },
    getFieldTypeLabel(fieldType) {
      const fieldObject = this.hubspotFieldTypes?.find((field) => field.value === fieldType);
      return fieldObject?.label;
    },
    getRequestOptions(method, data) {
      const config = {
        headers: {
          Authorization: `Bearer ${this.apiToken}`,
          "Content-Type": "application/json",
        },
      };
      if (method === METHOD_POST) {
        return {
          method: method,
          headers: config.headers,
          body: data ? JSON.stringify(data) : "",
        };
      } else {
        return {
          method: method,
          headers: config.headers,
        };
      }
    },
  },
};
</script>
