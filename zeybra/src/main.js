import { createApp } from "vue";
import App from "./App.vue";
import "@fortawesome/fontawesome-free/js/all.min.js";
import router from "./router";

createApp(App).use(router).mount("#app");
