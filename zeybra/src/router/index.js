import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../components/Home.vue";
import UIHome from "../views/UIHome.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/ui",
    name: "Frontend",
    component: UIHome,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
